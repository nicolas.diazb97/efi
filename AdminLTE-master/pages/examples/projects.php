<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Alianza Kennedy | EFI</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<?php
require_once 'model/Role.php';
require_once 'model/Permission.php';
require_once 'model/PrivilegedUser.php';
include("db.php");

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

$u = isset($_SESSION['user']->username) ? PrivilegedUser::getByUsername($_SESSION['user']->username) : false;
if (!$u) {
    header("Location: login.php", true, 302);
    die("<h2>302 Redirected</h2><p><a href='login.php'>Login</a> to continue.</p>");
}

if (!$u->hasPrivilege('view_role')) {
    header("Location: index.php", true, 403);
    die ("<h2>403 Forbidden</h2><p>You are not allowed here. Please contact administrator <a href='mailto:admin@company.com'>admin@company.com</a>.</p>");
}


if (!$u->hasPrivilege('add_user')) {
  header("Location: index.php", true, 403);
  die ("<h2>403 Forbidden</h2><p>You are not allowed here. Please contact administrator <a href='mailto:admin@company.com'>admin@company.com</a>.</p>");
}

if (isset($_POST['update'])) 
{
    $query = "SELECT * FROM curr_notes ";
    $result_tasks = mysqli_query($conn, $query);    
    while($row = mysqli_fetch_assoc($result_tasks)) {
      $firstNote =$row['first_note'];
      $secondNote =$row['second_note'];
      $currfinalNote = ($firstNote +$secondNote)/2;
      $currsubjectid = $row['subject_id'];
      $nextsubjectid = $row['subject_id'] + 1;
      $tempcurruserid = $row['user_id'];
        if($currfinalNote >=3){
          if($currsubjectid<=8){
            $query = "UPDATE curr_notes set subject_id = '$nextsubjectid' WHERE user_id=$tempcurruserid";
            mysqli_query($conn, $query);
          } 
        }
        $querydel = "DELETE FROM usernotes WHERE subject_id = $currsubjectid AND user_id=$tempcurruserid ";
        mysqli_query($conn, $querydel);
        $query2 ="INSERT INTO usernotes (user_id, subject_id, result) 
        VALUES ('$tempcurruserid','$currsubjectid','$currfinalNote')";
        mysqli_query($conn, $query2);
        
      }
      $finalquery = "UPDATE curr_notes set first_note = 0, result = 0,  second_note = 0 ";
      mysqli_query($conn, $finalquery);
      
      echo '<script language="javascript">';
      echo 'alert("Ciclo terminado.")';
      echo '</script>';
}

    
?>
<script>
  
  function funcion(){
    console.log("esto pasaaa");
    
    // 
  }
</script>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-sm-inline-block">
        <a href="logout.php" class="nav-link">Salir</a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="../../index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="../../dist/img/logoefiwh.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">E.F.I</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x1602.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="index.php" class="d-block"><?php echo $_SESSION['user']->username; ?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->             
          <!-- <li class="nav-item">
            <a href="../tables/data.php" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Notas
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../calendar.php" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Calendario
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li> -->
          <li class="nav-item menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Plan de estudios
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="projects.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cursos</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Usuarios
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../examples/usuarios.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Miembros</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cursos</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Curso</th>
                    <th>Profesor</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                      
                  <?php
                  $currid = $_SESSION['user']->user_id;
          $query = "SELECT * FROM subjects ";
          $result_tasks = mysqli_query($conn, $query);    
          while($row = mysqli_fetch_assoc($result_tasks)) {
            $tempsubjectname =$row['subjectname'];
            $tempsubjectid =$row['subjectid'];
            $query2 = "SELECT users.*, teachers.*
          FROM users LEFT JOIN teachers ON teachers.user_id = users.user_id WHERE teachers.subject_id = '$tempsubjectid'  ";
          $result_tasks2 = mysqli_query($conn, $query2);    
          
          
//$result = $conn -> query($query2);

// Numeric array
//$row2 = $result -> fetch_array(MYSQLI_NUM);
//echo count(" " . $row2);
          while($row2 = mysqli_fetch_assoc($result_tasks2)) { 
            $tempteachername =$row2['username'];
            // $secondNote =$row['second_note'];
            ?>

                  <tr>
                    <td><?php echo $tempsubjectname; ?></td>
                    <td><?php echo $tempteachername ; ?></td>
                    
                    <td>
                      <a class="btn btn-primary btn-sm" href="project-detail.php?uid=<?php echo $tempsubjectid; ?>">
                          <i class="fas fa-folder">
                          </i>
                          Ver
                      </a>
                    </td>
                  </tr>
                <?php }} ?>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
       
      <!-- /.card -->
            <!-- Button trigger modal -->
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
  Cerrar ciclo
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ESTA A PUNTO DE TERMINAR EL CICLO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      ¿Esta seguro de dar por terminado el ciclo? <br><br>Todos los estudiantes con notas inferiores a 3.0 repetiran el curso, esta acción no se puede revertir.
      </div>
      <div class="modal-footer">        
        <form action="projects.php?uid=<?php echo '1'; ?>" method="POST">

          <input type="submit" value="Terminar" class="btn btn-danger" name="update"></input>
        </form>
        <button type="button" class="btn btn-success" data-dismiss="modal" >Cancelar</button>
      </div>
    </div>
  </div>
</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0
    </div>
    <strong>Ministerio de Comunicaciones &copy; 2022 <a href="https://alianzakennedy.com/">Alianza Kennedy</a>.</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>

<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../../plugins/jszip/jszip.min.js"></script>
<script src="../../plugins/pdfmake/pdfmake.min.js"></script>
<script src="../../plugins/pdfmake/vfs_fonts.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
