<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Alianza Kennedy | EFI</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini"><?php
require_once 'model/Role.php';
require_once 'model/Permission.php';
require_once 'model/PrivilegedUser.php';
include("db.php");

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

$u = isset($_SESSION['user']->username) ? PrivilegedUser::getByUsername($_SESSION['user']->username) : false;
if (!$u) {
    header("Location: login.php", true, 302);
    die("<h2>302 Redirected</h2><p><a href='login.php'>Login</a> to continue.</p>");
}

if (!$u->hasPrivilege('view_role')) {
    header("Location: index.php", true, 403);
    die ("<h2>403 Forbidden</h2><p>You are not allowed here. Please contact administrator <a href='mailto:admin@company.com'>admin@company.com</a>.</p>");
}

$editableuid = $_GET['uid'];
$username = '';
$email= '';
$studentid= '';
$phone= '';
 


if (isset($_POST['update'])) {
  
  $username = $_POST['name'];
  $email= $_POST['correo'];
  $studentid= $_POST['id'];
  $phone = $_POST['phone'];

  $query = "UPDATE users set username = '$username', email = '$email', studentid = '$studentid', phone  = '$phone' WHERE user_id=$editableuid";
  mysqli_query($conn, $query);
if(!empty($_POST['subject'])) {
  $currsubject = $_POST['subject'];
  

  $query4 ="DELETE FROM teachers WHERE user_id='$editableuid'";
  mysqli_query($conn, $query4);

  $query5 ="DELETE FROM teachers WHERE subject_id='$currsubject'";
  mysqli_query($conn, $query5);

  $query3 ="INSERT INTO teachers (user_id, subject_id, result) 
  VALUES ('$editableuid','$currsubject',0)";
  mysqli_query($conn, $query3);
  echo $editableuid;
  echo "-";
  echo $currsubject;
  }
  
  // header('Location: index.php');
}

$user = User::seteditableuser($editableuid);
$_SESSION['editableuser'] = $user;

?>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="../../index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- Right navbar links -->
   
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="../../dist/img/logoefiwh.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">E.F.I</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x1602.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="index.php" class="d-block"><?php echo $_SESSION['user']->username; ?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->             
         
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Plan de estudios
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="projects.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cursos</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Usuarios
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../examples/usuarios.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Miembros</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Editar Usuario</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
              <!-- <li class="breadcrumb-item active">Project Edit</li> -->
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">General</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
                <form action="usuarios-edit-teachers.php?uid=<?php echo $_GET['uid']; ?>" method="POST">
              <div class="form-group">
                <label for="inputName">Nombre</label>
                <input type="text" id="inputName" class="form-control" name="name" value="<?php echo $_SESSION['editableuser']->username; ?> ">
              </div>
              <div class="form-group">
                <label for="inputName">Correo</label>
                <input type="text" id="inputName" class="form-control" name="correo" value="<?php echo $_SESSION['editableuser']->email; ?> ">
              </div>
              <div class="form-group">
                <label for="inputName">Contraseña</label>
                <input type="password" id="inputName" class="form-control" name="password">
              </div>
              <div class="form-group">
                <label for="inputName">Cargo</label>
                <select name="role" class="form-control custom-select">
                  <option value="" selected disabled>Selecciona uno</option>
                  <option value="2">Estudiante</option>
                  <option value="3" >Profesor</option>
                  <option value="4" >Administrador</option>
                </select>
              </div>
              <div class="form-group">
                <label for="inputClientCompany">Cedula</label>
                <input type="text" id="inputClientCompany" class="form-control" name="id" value="<?php echo $_SESSION['editableuser']->studentid; ?> ">
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Celular</label>
                <input type="text" id="inputProjectLeader" class="form-control" name="phone" value="<?php echo $_SESSION['editableuser']->phone; ?> "> 
              </div>
           </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Cursos</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
                <label for="inputName">Curso Actual</label>
                <select name="subject" class="form-control custom-select">
                  <option value="" selected disabled>Selecciona uno</option>
                  <option value="1">Membresia</option>
                  <option value="2" >Discipulado</option>
                  <option value="3" >Fruto y Dones</option>
                  <option value="4" >Grupos de vida</option>
                  <option value="5" >Doctrinas I</option>
                  <option value="6" >Doctrinas II</option>
                  <option value="7" >Consejeria</option>
                  <option value="8" >Historia de la Alianza</option>
                </select>
            <div class="row">
              <div class="col-12">
                <a href="#" class="btn btn-secondary">Cancel</a>
                <input type="submit" value="Guardar Cambios" class="btn btn-success float-right" name="update">
              </div>
            </div>
            </form>
            </div>


            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <!-- <a href="#" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Guardar Cambios" class="btn btn-success float-right" name="update"> -->
        </div>
      </div>
    </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0
    </div>
    <strong>Ministerio de Comunicaciones &copy; 2022 <a href="https://alianzakennedy.com/">Alianza Kennedy</a>.</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
