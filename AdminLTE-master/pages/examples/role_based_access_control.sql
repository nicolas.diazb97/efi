-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-02-2022 a las 22:40:41
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `role_based_access_control`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curr_notes`
--

CREATE TABLE `curr_notes` (
  `note_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) NOT NULL,
  `first_note` double NOT NULL,
  `result` float NOT NULL,
  `second_note` double NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `curr_notes`
--

INSERT INTO `curr_notes` (`note_id`, `user_id`, `subject_id`, `first_note`, `result`, `second_note`, `id`) VALUES
(0, 8, 4, 3.5, 0, 2.5, 1),
(0, 33, 4, 5, 0, 4.8, 2),
(0, 26, 4, 4, 0, 1.5, 3),
(0, 9, 4, 5, 0, 5, 4),
(0, 10, 4, 0, 0, 0, 5),
(0, 27, 4, 0, 0, 0, 6),
(0, 29, 4, 0, 0, 0, 7),
(0, 30, 4, 0, 0, 0, 8),
(0, 34, 4, 0, 0, 0, 9),
(0, 28, 4, 0, 0, 0, 10),
(0, 35, 4, 0, 0, 0, 11),
(0, 32, 2, 4.2, 0, 1.8, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `perm_id` int(10) UNSIGNED NOT NULL,
  `perm_desc` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`perm_id`, `perm_desc`) VALUES
(5, 'add_accounts_info'),
(17, 'add_billing_info'),
(21, 'add_marketing_info'),
(37, 'add_permission'),
(33, 'add_role'),
(13, 'add_sales_info'),
(1, 'add_user'),
(7, 'delete_accounts_info'),
(11, 'delete_balance_sheet'),
(19, 'delete_billing_info'),
(23, 'delete_marketing_info'),
(39, 'delete_permission'),
(27, 'delete_reports'),
(35, 'delete_role'),
(15, 'delete_sales_info'),
(31, 'delete_system_alert'),
(3, 'delete_user'),
(41, 'edit_subject_notes'),
(9, 'generate_balance_sheet'),
(25, 'generate_reports'),
(29, 'generate_system_alert'),
(6, 'update_accounts_info'),
(10, 'update_balance_sheet'),
(18, 'update_billing_info'),
(22, 'update_marketing_info'),
(38, 'update_permission'),
(26, 'update_reports'),
(34, 'update_role'),
(14, 'update_sales_info'),
(30, 'update_system_alert'),
(2, 'update_user'),
(8, 'view_accounts_info'),
(12, 'view_balance_sheet'),
(20, 'view_billing_info'),
(24, 'view_marketing_info'),
(40, 'view_permission'),
(28, 'view_reports'),
(36, 'view_role'),
(16, 'view_sales_info'),
(32, 'view_system_alert'),
(4, 'view_user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(4, 'admin'),
(2, 'estudiante'),
(9, 'finance'),
(5, 'it'),
(8, 'marketing'),
(1, 'masterAdmin'),
(3, 'profesor'),
(7, 'sales'),
(6, 'staff');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_perm`
--

CREATE TABLE `role_perm` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `perm_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `role_perm`
--

INSERT INTO `role_perm` (`role_id`, `perm_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 32),
(2, 36),
(3, 21),
(3, 23),
(3, 24),
(3, 32),
(3, 36),
(3, 41),
(4, 5),
(4, 6),
(4, 7),
(4, 8),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 32),
(4, 36),
(5, 29),
(5, 30),
(5, 31),
(5, 32),
(5, 36),
(6, 25),
(6, 26),
(6, 27),
(6, 28),
(6, 32),
(6, 36);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subjects`
--

CREATE TABLE `subjects` (
  `subjectid` int(11) NOT NULL,
  `subjectname` varchar(255) NOT NULL,
  `consecutive` int(40) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `subjects`
--

INSERT INTO `subjects` (`subjectid`, `subjectname`, `consecutive`, `user_id`) VALUES
(1, 'Membresia', 1, 0),
(2, 'Discipulado', 2, 0),
(3, 'Fruto y Dones', 3, 0),
(4, 'Grupos de vida', 4, 0),
(5, 'Doctrinas', 5, 0),
(6, 'Doctrinas 2', 6, 0),
(7, 'Consejeria', 7, 0),
(8, 'Historia de la Alianza', 8, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teachers`
--

CREATE TABLE `teachers` (
  `teacher_id` int(10) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) NOT NULL,
  `result` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `teachers`
--

INSERT INTO `teachers` (`teacher_id`, `user_id`, `subject_id`, `result`) VALUES
(1, 36, 2, 0),
(2, 32, 4, 0),
(3, 8, 2, 0),
(21, 37, 4, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usernotes`
--

CREATE TABLE `usernotes` (
  `note_id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `subject_id` int(10) NOT NULL,
  `result` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usernotes`
--

INSERT INTO `usernotes` (`note_id`, `user_id`, `subject_id`, `result`) VALUES
(91, 32, 1, 5),
(92, 32, 2, 3),
(93, 32, 3, 3),
(94, 32, 4, 3),
(95, 32, 5, 4),
(96, 32, 6, 3),
(97, 32, 7, 3),
(98, 32, 8, 1),
(211, 8, 1, 4),
(212, 8, 2, 3),
(213, 8, 3, 3),
(214, 8, 4, 2),
(219, 30, 1, 5),
(220, 30, 2, 5),
(221, 30, 3, 5),
(222, 30, 4, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `studentid` int(40) NOT NULL,
  `phone` int(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `studentid`, `phone`) VALUES
(8, 'Juanete Diaz Bernal     ', 'e4bea4b318f121c4c26134d40deae7653e97fdf5f0dc4929ea910d14e88260315c24431c8b60b28e3d683d583c8e02d95db06f87c81ad348b9f7256458b8bd27', 'juandiazb8@gmail.com      ', 2147483647, 755),
(9, 'Nicolas Test', '7cc8141b6464e214e540fdeb4a9f40cb38f30648cfc539d5df6b0e9038781d035e1b0f6de08d86388639844ab00698b18f850ad639cc668feb4f0fc8e6fcd1ad', 'nicolas.diazb97@gmail.com', 1030682592, 2147483647),
(10, 'Martinez', '320806af5eb5810ba093caec26c4d18dfaff6fca155aa1c3d9bbaa0962d7b59154affec03d9371ebe80bce32b93e1e82fb4fe8a63b6427acfdbc3ca5a5803101', 'nico@gmail.com', 456489, 123),
(26, 'esteban yarpaz', '7cc8141b6464e214e540fdeb4a9f40cb38f30648cfc539d5df6b0e9038781d035e1b0f6de08d86388639844ab00698b18f850ad639cc668feb4f0fc8e6fcd1ad', 'yarpaz@gmail.com', 2147483647, 2147483647),
(27, 'testing', '7cc8141b6464e214e540fdeb4a9f40cb38f30648cfc539d5df6b0e9038781d035e1b0f6de08d86388639844ab00698b18f850ad639cc668feb4f0fc8e6fcd1ad', 'test@gmail.com', 2147483647, 2147483647),
(28, 'testing2', '320806af5eb5810ba093caec26c4d18dfaff6fca155aa1c3d9bbaa0962d7b59154affec03d9371ebe80bce32b93e1e82fb4fe8a63b6427acfdbc3ca5a5803101', 'test2@gmail.com', 2147483647, 2147483647),
(29, 'Cams Sanchez ', '320806af5eb5810ba093caec26c4d18dfaff6fca155aa1c3d9bbaa0962d7b59154affec03d9371ebe80bce32b93e1e82fb4fe8a63b6427acfdbc3ca5a5803101', 'cami@gmail.com ', 2147483641, 2147483647),
(30, 'Norberto Diaz ', '320806af5eb5810ba093caec26c4d18dfaff6fca155aa1c3d9bbaa0962d7b59154affec03d9371ebe80bce32b93e1e82fb4fe8a63b6427acfdbc3ca5a5803101', 'norberto@gmail.com ', 79419603, 2147483647),
(32, 'Nico Diaz                      ', '7cc8141b6464e214e540fdeb4a9f40cb38f30648cfc539d5df6b0e9038781d035e1b0f6de08d86388639844ab00698b18f850ad639cc668feb4f0fc8e6fcd1ad', 'nicolas.diazb@utadeo.edu.co                       ', 1030682592, 2147483647),
(33, 'Sandra Bernal', '320806af5eb5810ba093caec26c4d18dfaff6fca155aa1c3d9bbaa0962d7b59154affec03d9371ebe80bce32b93e1e82fb4fe8a63b6427acfdbc3ca5a5803101', 'sandra@gmail.com', 51643757, 2147483647),
(34, 'yenny', '320806af5eb5810ba093caec26c4d18dfaff6fca155aa1c3d9bbaa0962d7b59154affec03d9371ebe80bce32b93e1e82fb4fe8a63b6427acfdbc3ca5a5803101', 'yenny@gmail.com', 2147483647, 345678912),
(35, 'adminAlianza', '320806af5eb5810ba093caec26c4d18dfaff6fca155aa1c3d9bbaa0962d7b59154affec03d9371ebe80bce32b93e1e82fb4fe8a63b6427acfdbc3ca5a5803101', 'alianza@gmail.com', 2147483647, 2147483647),
(36, 'Alex Corvis', '320806af5eb5810ba093caec26c4d18dfaff6fca155aa1c3d9bbaa0962d7b59154affec03d9371ebe80bce32b93e1e82fb4fe8a63b6427acfdbc3ca5a5803101', 'corvis@alianza.com', 2147483647, 2147483647),
(37, 'John Jairo                      ', '320806af5eb5810ba093caec26c4d18dfaff6fca155aa1c3d9bbaa0962d7b59154affec03d9371ebe80bce32b93e1e82fb4fe8a63b6427acfdbc3ca5a5803101', 'john@efi.com                                  ', 4578, 77);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_role`
--

CREATE TABLE `user_role` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(8, 3),
(9, 3),
(10, 3),
(28, 2),
(29, 4),
(30, 4),
(32, 4),
(33, 3),
(34, 3),
(35, 2),
(36, 3),
(37, 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `curr_notes`
--
ALTER TABLE `curr_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_note12` (`subject_id`),
  ADD KEY `user_note22` (`user_id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`perm_id`),
  ADD UNIQUE KEY `perm_desc` (`perm_desc`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `role_name` (`role_name`);

--
-- Indices de la tabla `role_perm`
--
ALTER TABLE `role_perm`
  ADD UNIQUE KEY `role_id_2` (`role_id`,`perm_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `role_perm_ibfk_2` (`perm_id`);

--
-- Indices de la tabla `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subjectid`);

--
-- Indices de la tabla `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`teacher_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `subjectid` (`subject_id`);

--
-- Indices de la tabla `usernotes`
--
ALTER TABLE `usernotes`
  ADD PRIMARY KEY (`note_id`),
  ADD KEY `user_note` (`user_id`),
  ADD KEY `usernote` (`subject_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD UNIQUE KEY `user_id_2` (`user_id`,`role_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_role_ibfk_2` (`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `curr_notes`
--
ALTER TABLE `curr_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `perm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subjectid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `teachers`
--
ALTER TABLE `teachers`
  MODIFY `teacher_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `usernotes`
--
ALTER TABLE `usernotes`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `curr_notes`
--
ALTER TABLE `curr_notes`
  ADD CONSTRAINT `user_note12` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subjectid`),
  ADD CONSTRAINT `user_note22` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Filtros para la tabla `role_perm`
--
ALTER TABLE `role_perm`
  ADD CONSTRAINT `role_perm_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  ADD CONSTRAINT `role_perm_ibfk_2` FOREIGN KEY (`perm_id`) REFERENCES `permissions` (`perm_id`);

--
-- Filtros para la tabla `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `subjectid` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subjectid`),
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Filtros para la tabla `usernotes`
--
ALTER TABLE `usernotes`
  ADD CONSTRAINT `user_note` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `usernote` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subjectid`);

--
-- Filtros para la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
