<?php
require_once 'Database.php';

class User
{
    public $user_id;
    public $username;
    public $email;
    public $studentid;
    public $phone;
    public $cursos = Array();
    public $cursosDefault = Array();
    public $cursosresultados = Array();

    public function __construct()
    {

    }

    public static function add($username, $password, $email)
    {
        $db = new Database();

        $sql = "SELECT count(user_id) AS count, user_id FROM users WHERE username = :username";
        $sth = $db->conn->prepare($sql);
        $sth->execute(array(":username" => $username));
        $result = $sth->fetchAll();

        if (!empty($result) && $result[0]["count"] === 0) {
            $sql2 = "INSERT INTO users (username, password, email) VALUES (:username, SHA2(:password, 512), :email)";
            $sth2 = $db->conn->prepare($sql2);
            $sth2->execute(array(":username" => $username, ":password" => $password, ":email" => $email));
            return $sth->rowCount();
        } else {
            return false;
        }
    }

    public static function deleteByUsername($username)
    {
        $db = new Database();

        $sql = "DELETE FROM users WHERE username = :username";
        $sth = $db->conn->prepare($sql);
        $sth->execute(array(":username" => $username));
        return $sth->rowCount();
    }

    public static function deleteByEmail($email)
    {
        $db = new Database();

        $sql = "DELETE FROM users WHERE email = :email";
        $sth = $db->conn->prepare($sql);
        $sth->execute(array(":email" => $email));
        return $sth->rowCount();
    }

    protected static function getByUsername($username)
    {
        $db = new Database();

        $sql = "SELECT user_id, username, email, FROM users WHERE username = :username";
        $sth = $db->conn->prepare($sql);
        $sth->execute(array(":username" => $username));
        $result = $sth->fetchAll();

        if (!empty($result)) {
            $user = new User();
            $user->user_id = $result[0]["user_id"];
            $user->username = $username;
            $user->email = $result[0]["email"];
            return $user;
        } else {
            return false;
        }
    }

    public static function validate($email, $password)
    {
        $db = new Database();
        $sql = "SELECT user_id, username, email, studentid, phone FROM users WHERE email = '$email' AND password = SHA2('$password', 512)";
        $sth = $db->conn->prepare($sql);
        $mysqli = new mysqli("localhost", "root", "Nico12las", "role_based_access_control");
        $mysqli->set_charset("utf8");
        $curr_user_id;
        $user = new User();
        $result2 = $mysqli->query("SELECT user_id, username, email, studentid, phone FROM users WHERE email = '$email' AND password = SHA2('$password', 512)");
        while($f = $result2->fetch_object()){
            $user->username = $f->username;
            $user->user_id = $f->user_id;
            $user->email = $f->email;
            $user->studentid = $f->studentid;
            $user->phone = $f->phone;
            $curr_user_id = $f->user_id;
        }
        
        $result3 = $mysqli->query("SELECT * FROM usernotes WHERE user_id ='$curr_user_id'");
        while($f = $result3->fetch_object()) {
            $user->cursosresultados[]= $f->result;
            $result4 = $mysqli->query("SELECT subjectname FROM subjects WHERE subjectid ='$f->subject_id'");
            while($f2 = $result4->fetch_object()) {
                $user->cursos[]= $f2->subjectname;
            }
        }
        $result4 = $mysqli->query("SELECT subjectname FROM subjects");
        while($f = $result4->fetch_object()){
            
            $user->cursosDefault[]= $f->subjectname;
        }
        
        // echo $user->cursosresultados[0];
        return $user;
        // echo $result3;
        // $sth->execute(array(":email" => $email, ":password" => $password));
        // $result = $sth->fetchAll();

        // if (!empty($result2)) {
        //     $user = new User();
        //     $user->user_id = $result2[0]["user_id"];
        //     $user->username = $result2[0]["username"];
        //     $user->email = $result2[0]["email"];
        //     return $user;
        // }
        echo "results";
        return false;
    }

    public static function seteditableuser($uid)
    {
        $db = new Database();
        $sql = "SELECT user_id, username, email, studentid, phone FROM users WHERE user_id = '$uid' ";
        $sth = $db->conn->prepare($sql);
        $mysqli = new mysqli("localhost", "root", "Nico12las", "role_based_access_control");
        $mysqli->set_charset("utf8");
        $curr_user_id;
        $user = new User();
        $result2 = $mysqli->query("SELECT user_id, username, email, studentid, phone FROM users WHERE user_id = '$uid' ");
        while($f = $result2->fetch_object()){
            $user->username = $f->username;
            $user->user_id = $f->user_id;
            $user->email = $f->email;
            $user->studentid = $f->studentid;
            $user->phone = $f->phone;
            $curr_user_id = $f->user_id;
        }
        
        $result3 = $mysqli->query("SELECT * FROM usernotes WHERE user_id ='$curr_user_id'");
        while($f = $result3->fetch_object()) {
            $user->cursosresultados[]= $f->result;
            $result4 = $mysqli->query("SELECT subjectname FROM subjects WHERE subjectid ='$f->subject_id'");
            while($f2 = $result4->fetch_object()) {
                $user->cursos[]= $f2->subjectname;
            }
        }
        $result4 = $mysqli->query("SELECT subjectname FROM subjects");
        while($f = $result4->fetch_object()){
            
            $user->cursosDefault[]= $f->subjectname;
        }
        
        // echo $user->cursosresultados[0];
        return $user;
        // echo $result3;
        // $sth->execute(array(":email" => $email, ":password" => $password));
        // $result = $sth->fetchAll();

        // if (!empty($result2)) {
        //     $user = new User();
        //     $user->user_id = $result2[0]["user_id"];
        //     $user->username = $result2[0]["username"];
        //     $user->email = $result2[0]["email"];
        //     return $user;
        // }
        echo "results";
        return false;
    }
}
