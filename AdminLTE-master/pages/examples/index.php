
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Alianza Kennedy | EFI</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">

<?php
require_once 'model/Role.php';
require_once 'model/Permission.php';
require_once 'model/PrivilegedUser.php';
include("db.php");

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

$u = isset($_SESSION['user']->username) ? PrivilegedUser::getByUsername($_SESSION['user']->username) : false;
if (!$u) {
    header("Location: login.php", true, 302);
    die("<h2>302 Redirected</h2><p><a href='login.php'>Login</a> to continue.</p>");
}

if (!$u->hasPrivilege('view_role')) {
  header("Location: index.php", true, 403);
  die ("<h2>403 Forbidden</h2><p>You are not allowed here. Please contact administrator <a href='mailto:admin@company.com'>admin@company.com</a>.</p>");
}
if ($u->hasPrivilege('add_user')) {
  ob_start();
  header('Location: projects.php');
  ob_end_flush();
  die();
}

if ($u->hasPrivilege('edit_subject_notes')) {
  ob_start();
  header('Location: index-teachers.php');
  ob_end_flush();
  die();
}


$subjectname= 'Inactivo';
$curridf =$_SESSION['user']->user_id;

$queryf = "SELECT subject_id FROM curr_notes WHERE user_id = '$curridf' ";
$result_tasks = mysqli_query($conn, $queryf);    
while($row = mysqli_fetch_assoc($result_tasks)) {
  $tempsubid =$row['subject_id'];
  $queryf2 = "SELECT subjectname FROM subjects WHERE subjectid = '$tempsubid' ";
  $result_tasksf = mysqli_query($conn, $queryf2);  

  while($row = mysqli_fetch_assoc($result_tasksf)) {
    $subjectname =$row['subjectname'];
  }  
}


//require_once 'menu.php';
?>

<?php
// insert roles
if ($u->hasPrivilege('add_role')) {
    $roles = ['admin', 'sales', 'marketing', 'finance', 'it', 'staff'];
    $count = 0;
    foreach ($roles as $role) {
        $count += Role::insertRole($role);
    }
//    echo "<div>{$count} role(s) inserted.</div>";
}

// insert permissions
if ($u->hasPrivilege('add_permission')) {
    $perms = [
        'add_user', 'update_user', 'delete_user', 'view_user', // 1-4
        'add_accounts_info', 'update_accounts_info', 'delete_accounts_info', 'view_accounts_info', // 5-8
        'generate_balance_sheet', 'update_balance_sheet', 'delete_balance_sheet', 'view_balance_sheet', // 9-12
        'add_sales_info', 'update_sales_info', 'delete_sales_info', 'view_sales_info', // 13-16
        'add_billing_info', 'update_billing_info', 'delete_billing_info', 'view_billing_info', // 17-20
        'add_marketing_info', 'update_marketing_info', 'delete_marketing_info', 'view_marketing_info', // 21-24
        'generate_reports', 'update_reports', 'delete_reports', 'view_reports', // 25-28
        'generate_system_alert', 'update_system_alert', 'delete_system_alert', 'view_system_alert', // 29-32
        'add_role', 'update_role', 'delete_role', 'view_role', // 33-36
        'add_permission', 'update_permission', 'delete_permission', 'view_permission', // 37-40
    ];
    $count = 0;
    foreach ($perms as $perm_desc) {
        $count += Permission::insertPerm($perm_desc);
    }
//    echo "<div>{$count} permission(s) inserted.</div>";
}

// assign roles to users
if ($u->hasPrivilege('add_role') && $u->hasPrivilege('add_permission')) {
    $role_perms = [
        ['role_id' => 1, 'perm_ids' => [1, 2, 3, 4, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44]],
        ['role_id' => 2, 'perm_ids' => [9, 10, 11, 12, 13, 14, 15, 16, 32, 36]],
        ['role_id' => 3, 'perm_ids' => [21, 22, 23, 24, 32, 36]],
        ['role_id' => 4, 'perm_ids' => [5, 6, 7, 8, 17, 18, 19, 20, 32, 36]],
        ['role_id' => 5, 'perm_ids' => [29, 30, 31, 32, 36]],
        ['role_id' => 6, 'perm_ids' => [25, 26, 27, 28, 32, 36]],
    ];
    $count = 0;
    foreach ($role_perms as $role_perm) {
//        $count += Role::insertRolePerms($role_perm['role_id'], $role_perm['perm_ids']);
    }
//    echo "<div>{$count} permission(s) inserted on " . count($role_perms) . " role(s).</div>";
}

if ($u->hasPrivilege('add_user') && $u->hasPrivilege('add_role')) {
    $count = Role::insertUserRoles(2, [2, 3]);
//    echo "<div>{$count} user role(s) inserted.</div>";
}
?>
<div>
</div>
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <!-- <a href="../../index3.html" class="nav-link">Home</a> -->
      </li>
      <li class="nav-item d-sm-inline-block">
        <a href="logout.php" class="nav-link">Salir</a>
      </li>
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="../../dist/img/logoefiwh.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">E.F.I</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x1602.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="index.php" class="d-block"><?php echo $_SESSION['user']->username; ?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->             
          <li class="nav-item">
            <a href="../tables/data.php" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Notas
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../calendar.php" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Calendario
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Plan de estudios
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../examples/projects.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cursos</p>
                </a>
              </li>
            </ul>
          </li> -->
          
          <!-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Usuarios
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../examples/usuarios.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Miembros</p>
                </a>
              </li>
            </ul>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Perfil</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../../dist/img/user2-160x1602.jpg"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?php echo $_SESSION['user']->username; ?></h3>

                <p class="text-muted text-center">Estudiante</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Curso</b> <a class="float-right"><?php echo $subjectname;?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Promedio</b> <a class="float-right">3.0</a>
                  </li>
                </ul>

                <a href="../tables/data.html" class="btn btn-primary btn-block"><b>Notas</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Acerca de la EFI</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-book mr-1"></i> Education</strong>

                <p class="text-muted">
                  Escuela Biblica
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Ubicación</strong>

                <p class="text-muted">Cra. 78a #33a Sur-43, Bogotá</p>

                <hr>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          
          <!-- /.col -->
          <div class="col-md-9">
            <div class="row">
            <?php
          $i = 0;
          while ($i < count($_SESSION['user']->cursosresultados)) {
              ?>
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box <?php if($_SESSION['user']->cursosresultados[$i] < 3){echo "bg-danger";} if($_SESSION['user']->cursosresultados[$i] >= 3 && $_SESSION['user']->cursosresultados[$i] < 4){echo "bg-info";} if($_SESSION['user']->cursosresultados[$i] >= 4){echo "bg-success";} ?>">
                  <div class="inner">
                    <h4><?php echo $_SESSION['user']->cursos[$i]; ?></h4>
    
                    <p>Nota: <?php echo $_SESSION['user']->cursosresultados[$i]; ?></p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-bag"></i>
                  </div>
                  <a href="#" class="small-box-footer">Notas <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <?php $i++; } 
            ?>
              <!-- ./col -->
              
              <!-- ./col -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
                  <!-- /.tab-pane -->
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0
    </div>
    <strong>Ministerio de Comunicaciones &copy; 2022 <a href="https://alianzakennedy.com/">Alianza Kennedy</a>.</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>

</body>
</html>
