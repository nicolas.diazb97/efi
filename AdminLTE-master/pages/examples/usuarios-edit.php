<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Alianza Kennedy | EFI</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini"><?php
require_once 'model/Role.php';
require_once 'model/Permission.php';
require_once 'model/PrivilegedUser.php';
include("db.php");

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

$u = isset($_SESSION['user']->username) ? PrivilegedUser::getByUsername($_SESSION['user']->username) : false;
if (!$u) {
    header("Location: login.php", true, 302);
    die("<h2>302 Redirected</h2><p><a href='login.php'>Login</a> to continue.</p>");
}

if (!$u->hasPrivilege('view_role')) {
    header("Location: index.php", true, 403);
    die ("<h2>403 Forbidden</h2><p>You are not allowed here. Please contact administrator <a href='mailto:admin@company.com'>admin@company.com</a>.</p>");
}

if (!$u->hasPrivilege('add_user')) {
  header("Location: index.php", true, 403);
  die ("<h2>403 Forbidden</h2><p>You are not allowed here. Please contact administrator <a href='mailto:admin@company.com'>admin@company.com</a>.</p>");
}
$editableuid = $_GET['uid'];
$username = '';
$email= '';
$studentid= '';
$phone= '';
 


if (isset($_POST['update'])) {
  
  $username = $_POST['name'];
  $email= $_POST['correo'];
  $studentid= $_POST['id'];
  $phone = $_POST['phone'];


  if (isset($_POST['checkbox']) && $_POST['checkbox'] == 'Yes'){



    $query = "UPDATE users set username = '$username', email = '$email', studentid = '$studentid', phone  = '$phone' WHERE user_id=$editableuid";
    mysqli_query($conn, $query);
    $querydel = "DELETE FROM usernotes WHERE user_id=$editableuid";
    mysqli_query($conn, $querydel);
    $_SESSION['message'] = 'Task Updated Successfully';
    $_SESSION['message_type'] = 'warning';
    
    
    $i = 0;
    while ($i < count($_SESSION['user']->cursosDefault)) {    
      
      $currindex =$i+1;
    $noteid = $_POST['curso' . $currindex];  
    if($noteid!="")
    {
      // $query2 ="UPDATE usernotes set subject_id = '$i', result = '$noteid' WHERE user_id=$editableuid";
      $query2 ="INSERT INTO usernotes (user_id, subject_id, result) 
      VALUES ('$editableuid','$currindex','$noteid')";
      mysqli_query($conn, $query2);
    }
    $i++;
    
    }


    $latestsubject=0;
    $latestgrade=0;
    $queryf = "SELECT * FROM usernotes WHERE user_id = '$editableuid' ";
$result_tasks1 = mysqli_query($conn, $queryf);    
while($row = mysqli_fetch_assoc($result_tasks1)) {
   if($row['subject_id'] >$latestsubject ) 
   {
     $latestsubject = $row['subject_id'];
     $latestgrade = $row['result'];
     }
     
    }
    if($latestsubject == 0){
      $latestsubject = 1;
    }
    else{
    if($latestgrade>=3){
      
      $latestsubject = $latestsubject +1;
    }
  }
    $querydel = "DELETE FROM curr_notes WHERE user_id=$editableuid";
    mysqli_query($conn, $querydel);
  $queryf2 = "INSERT INTO curr_notes (user_id, subject_id, first_note, result, second_note) 
     VALUES ('$editableuid','$latestsubject','0','0','0')";
$result_tasks2 = mysqli_query($conn, $queryf2);  
  }

  
  // header('Location: index.php');
}

$user = User::seteditableuser($editableuid);
$_SESSION['editableuser'] = $user;



$u2 = PrivilegedUser::getByUsername($_SESSION['editableuser']->username);
$headerstring = "Location: usuarios-edit-teachers.php?uid=";
$headerstring = $headerstring . $editableuid;
if ($u2->hasPrivilege('edit_subject_notes')) {
  ob_start();
  header($headerstring);
  ob_end_flush();
  die();
}

?>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-sm-inline-block">
        <a href="logout.php" class="nav-link">Salir</a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="../../index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="../../dist/img/logoefiwh.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">E.F.I</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../../dist/img/user2-160x1602.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="index.php" class="d-block"><?php echo $_SESSION['user']->username; ?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->             
          <!-- <li class="nav-item">
            <a href="../tables/data.php" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Notas
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../calendar.php" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Calendario
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li> -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Plan de estudios
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="projects.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cursos</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Usuarios
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../examples/usuarios.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Miembros</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Editar Usuario</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
              <!-- <li class="breadcrumb-item active">Project Edit</li> -->
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">General</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
                <form action="usuarios-edit.php?uid=<?php echo $_GET['uid']; ?>" method="POST">
              <div class="form-group">
                <label for="inputName">Nombre</label>
                <input type="text" id="inputName" class="form-control" name="name" value="<?php echo $_SESSION['editableuser']->username; ?> ">
              </div>
              <div class="form-group">
                <label for="inputName">Correo</label>
                <input type="text" id="inputName" class="form-control" name="correo" value="<?php echo $_SESSION['editableuser']->email; ?> ">
              </div>
              <div class="form-group">
                <label for="inputName">Contraseña</label>
                <input type="password" id="inputName" class="form-control" name="password">
              </div>
              <div class="form-group">
                <label for="inputName">Cargo</label>
                <select name="role" class="form-control custom-select">
                  <option value="" selected disabled>Selecciona uno</option>
                  <option value="2">Estudiante</option>
                  <option value="3" >Profesor</option>
                  <option value="4" >Administrador</option>
                </select>
              </div>
              <div class="form-group">
                <label for="inputClientCompany">Cedula</label>
                <input type="text" id="inputClientCompany" class="form-control" name="id" value="<?php echo $_SESSION['editableuser']->studentid; ?> ">
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Celular</label>
                <input type="text" id="inputProjectLeader" class="form-control" name="phone" value="<?php echo $_SESSION['editableuser']->phone; ?> "> 
              </div>
              <div class="form-check">
                <label class="form-check-label" for="flexCheckDefault" name="checkbox" >
               <input class="form-check-input" type="checkbox" value="Yes" id="flexCheckDefault" name="checkbox" <?php
               
               
    $queryf = "SELECT user_id FROM curr_notes WHERE user_id = '$editableuid' ";
    $result_tasks12 = mysqli_query($conn, $queryf);
               
               if(mysqli_num_rows($result_tasks12)>0){
                  echo "checked";
               } ?>>
                 Usuario activo
               </label>
              </div>
           </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Cursos</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
            <?php
          $i = 0;
          while ($i < count($_SESSION['user']->cursosDefault)) {
              ?>
              <div class="form-group">
                <label for="inputEstimatedBudget"><?php echo $_SESSION['user']->cursosDefault[$i]; ?></label>
                <input type="number" step="0.1" id="inputEstimatedBudget" class="form-control" name="curso<?php echo $i+1?>" value="<?php if(count( $_SESSION['editableuser']->cursosresultados)>$i) { echo $_SESSION['editableuser']->cursosresultados[$i];}?>" step="1">
              </div>
              
              <?php $i++; } 
            ?>
            <div class="row">
              <div class="col-12">
                <a href="#" class="btn btn-secondary">Cancel</a>
                <input type="submit" value="Guardar Cambios" class="btn btn-success float-right" name="update">
              </div>
            </div>
            </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <!-- <a href="#" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Guardar Cambios" class="btn btn-success float-right" name="update"> -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0
    </div>
    <strong>Ministerio de Comunicaciones &copy; 2022 <a href="https://alianzakennedy.com/">Alianza Kennedy</a>.</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
