<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'd674879bf0c3f6e09f31fabc72aa02656b07fddd',
        'name' => 'almasaeed2010/adminlte',
        'dev' => true,
    ),
    'versions' => array(
        'almasaeed2010/adminlte' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'd674879bf0c3f6e09f31fabc72aa02656b07fddd',
            'dev_requirement' => false,
        ),
    ),
);
